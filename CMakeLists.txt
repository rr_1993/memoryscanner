cmake_minimum_required(VERSION 3.10)

project(memorydumperc LANGUAGES C)

add_executable(memorydumperc main.c memorydumper.c processexplorer.c)

install(TARGETS memorydumperc RUNTIME DESTINATION bin)
