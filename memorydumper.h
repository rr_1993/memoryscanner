#ifndef __MEMORYDUMPER_H__
#define __MEMORYDUMPER_H__

/**
 * Author: Robert Hendriks
 * Description: Functions to dump the memory of a process to disk
 */

typedef struct MemoryRegion{
    unsigned long long start_address;
    unsigned long long end_addres;
};

/**
 * @brief Dumps a process inside memory to a memory block so it can be inspected
 * 
 * @param pid The process identifier of the running proccess
 * 
 * @param memblock A pointer to a pointer, this will be used to malloc space to and contains data
 * 
 * @return Returns -1 on error or the number of bytes on success
 * 
 * @warning This function attaches to the process and suspends it while dumping memory, the caller should free the memory block
 */
unsigned long long dump_process_to_memory_block(unsigned int pid, char **memblock);

/**
 * @brief Dumps a process inside memory to a file
 * 
 * @param pid The process identifier of the running procces
 * 
 * @param file_path An absolute or relative path to the output file
 * 
 * @warning This function attaches to the process and suspends it while dumping memory
 */
int dump_process_to_file(unsigned int pid, char* file_path);



#endif /* __MEMORYDUMPER_H__ */
