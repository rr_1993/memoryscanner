#include <stdio.h>
#include <stdlib.h>
#include "memorydumper.h"
#include "processexplorer.h"

int main(int argc, char *argv[])
{
    //get_all_pids();
    
    //Check if a pid has been given
    if (argc < 2){
        printf("%s <pid>\n", argv[0]);
        return 0;
    }
    
    //Dump the process to the output file
    dump_process_to_file(atoi(argv[1]), "my_dump.txt");
    return 0;
}
