
#include "processexplorer.h"
#include <dirent.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

//Private functions
int string_contains_only_numbers(char* string);


unsigned int* get_all_pids(){
    DIR *direcory = opendir("/proc");
    struct dirent *dirend_struct = NULL;
    if(!direcory) return NULL;
    
    //Malloc space for the array of pids
    unsigned int *pids = (unsigned int*)malloc(sizeof(unsigned int) * 512);
    unsigned int number_of_pids = 0;
    if(!pids){
        fprintf(stderr, "[-] Unable to malloc space, out of memory?");
        return NULL;
    }
    
    //Loop through directories
    while ((dirend_struct = readdir(direcory)) != NULL){
        if(dirend_struct->d_type == DT_DIR && string_contains_only_numbers(dirend_struct->d_name) == 0){
            pids[number_of_pids] = atoi(dirend_struct->d_name);
            number_of_pids += 1;
            if(number_of_pids >= 512) break;
        }
    }
    
    return pids;
}

char* get_name_of_process(unsigned int pid){
    char comm_path[256];
    sprintf(comm_path, "/proc/%i/comm", pid);
    FILE* comm_file = fopen(comm_path, "r");
    if(!comm_file){
        fprintf(stderr, "[-] Unable to open %s", comm_path);
        return NULL;
    }
    
    char *comm_name = (char*)malloc(sizeof(char) * 256);
    if(!comm_name){
        fprintf(stderr, "[-] Unable to malloc space, out of memory?");
        close(comm_file);
        return NULL;
    }
    fgets(comm_name, 256, comm_file);
    
    close(comm_file);
    return comm_name;
}

//Private
int string_contains_only_numbers(char* string)
{
    unsigned int length = strlen(string);
    for(unsigned int i = 0; i < length; i++){
        if(!isdigit(string[i])){
            return -1;
        }
    }
    
    return 0;
}
