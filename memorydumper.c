
#include "memorydumper.h"
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <stdio.h>

//test123


//Private functions
void detach_from_process(unsigned int pid);
void dump_memory_region_to_file(FILE* memory_file, FILE* output_file, unsigned long long start_address, unsigned long long length);
FILE* open_maps_file_of_process(unsigned int pid);
FILE* open_mem_file_of_process(unsigned int pid);
struct MemoryRegion* get_memory_regions_of_process(unsigned int pid, unsigned int* number_of_regions);

int dump_process_to_file(unsigned int pid, char* file_path)
{
    //Attach to proccess
    long result = ptrace(PTRACE_ATTACH, pid, NULL, NULL);
    if(result < 0){
        fprintf(stderr, "[-] Error attaching to process with pid: %i, errno: %i", pid, errno);
        return -1;
    }
    wait(NULL);
    
    //Open output_file
    FILE* output_file = fopen(file_path, "wb");
    if(!output_file){
        fprintf(stderr, "[-] Error opening outputfile %s", file_path);
        detach_from_process(pid);
        return -1;
    }
    
    //Open memfile
    FILE* mem_file = open_mem_file_of_process(pid);
    if(!mem_file){
        fprintf(stderr, "[-] Error opening mem file of process with pid %i", pid);
        detach_from_process(pid);
        return -1;
    }

    //Get all regions of this process
    unsigned int number_of_regions = 0;
    struct MemoryRegion *memory_regions = get_memory_regions_of_process(pid, &number_of_regions);
    if(!memory_regions){
        fprintf(stderr, "[-] Error fetching memory regions");
        detach_from_process(pid);
        return -1;
    }
    //Dump the memory regions
    for(unsigned int i = 0; i < number_of_regions; i++){
        dump_memory_region_to_file(mem_file, 
                                    output_file, 
                                   memory_regions[i].start_address, 
                                   (memory_regions[i].end_addres - memory_regions[i].start_address));
    }
    free(memory_regions);
    
    //Clean up
    close(mem_file);
    close(output_file);
    detach_from_process(pid);
    return 0;
}

struct MemoryRegion* get_memory_regions_of_process(unsigned int pid, unsigned int* number_of_regions)
{
    //Create a memory pool for the results (Best effort, we can always realloc ...)
    struct MemoryRegion* memory_regions = (struct MemoryRegion*) malloc(sizeof(struct MemoryRegion) * 512);
    if(!memory_regions){
        return NULL;
    }
 
    //Open the maps file
    FILE* maps_file = open_maps_file_of_process(pid);
    if(!maps_file){
        return NULL;
    }
    
    //Get regions
    *number_of_regions = 0;
    char line[256]; 
    while(fgets(line, 256, maps_file) != NULL){
        struct MemoryRegion region;
        unsigned long long start_address, end_address = 0;
        sscanf(line, "%llx-%llx", &region.start_address, &region.end_addres);
        memory_regions[*number_of_regions] = region;
        *number_of_regions += 1;
        
        if(*number_of_regions >= 512){
            //TODO: realloc, for now just skip
            break;
        }
    }
    
    return memory_regions;
}



/* Private */
void detach_from_process(unsigned int pid)
{
    ptrace(PTRACE_CONT, pid, NULL, NULL);
    ptrace(PTRACE_DETACH, pid, NULL, NULL);
}
 
/* Private */
void dump_memory_region_to_file(FILE* mem_file, FILE* output_file, unsigned long long start_address, unsigned long long length)
{
    unsigned long address;
    int pageLength = 4096;
    unsigned char page[pageLength];
    fseeko(mem_file, start_address, SEEK_SET);

    for (address=start_address; address < start_address + length; address += pageLength)
    {
        fread(&page, 1, pageLength, mem_file);
        fwrite(&page, 1, pageLength, output_file);
    }
}

/* Private */
FILE* open_mem_file_of_process(unsigned int pid){
    char mem_path[256];
    sprintf(mem_path, "/proc/%i/mem", pid);
    return fopen(mem_path, "r");
}

/* Private */
FILE* open_maps_file_of_process(unsigned int pid){
    char maps_path[256];
    sprintf(maps_path, "/proc/%i/maps", pid);
    return fopen(maps_path, "r");
}


