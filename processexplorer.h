#ifndef __PROCESSEXPLORER_H__
#define __PROCESSEXPLORER_H__

/**
 * @brief Get the pids of all running processes
 * 
 * @return Returns NULL on error, else returns a array on the heap with unsigned ints representing the pids
 * 
 * @warning Caller should free the returned int buffer, this function returns a maximum of 512 pids
 */
unsigned int* get_all_pids();

/**
 * @brief get the command name by pid
 * 
 * @warning Caller should free the returned char buffer
 */
char* get_name_of_process(unsigned int pid);






#endif /* __PROCESSEXPLORER_H__ */
